import pickle

import math
from scipy.misc import imsave
import numpy as np
from numpy import ma
from scipy.ndimage.filters import convolve
from scipy.ndimage.filters import correlate


def bilinear_interp(image, points):
    """Given an image and an array of row/col (Y/X) points, perform bilinear
    interpolation and return the pixel values in the image at those points."""
    points = np.asarray(points)
    if points.ndim == 1:
        points = points[np.newaxis]

    valid = np.all(points < [image.shape[0] - 1, image.shape[1] - 1], axis=-1)
    valid *= np.all(points >= 0, axis=-1)
    valid = valid.astype(np.float32)
    points = np.minimum(points, [image.shape[0] - 2, image.shape[1] - 2])
    points = np.maximum(points, 0)

    fpart, ipart = np.modf(points)
    tl = ipart.astype(np.int32)
    br = tl + 1
    tr = np.concatenate([tl[..., 0:1], br[..., 1:2]], axis=-1)
    bl = np.concatenate([br[..., 0:1], tl[..., 1:2]], axis=-1)

    b = fpart[..., 0:1]
    a = fpart[..., 1:2]

    top = ((1 - a) * image[tl[..., 0], tl[..., 1]] +
           a * image[tr[..., 0], tr[..., 1]])
    bot = ((1 - a) * image[bl[..., 0], bl[..., 1]] +
           a * image[br[..., 0], br[..., 1]])

    return ((1 - b) * top + b * bot) * valid[..., np.newaxis]


def translate(image, displacement):
    """Takes an image and a displacement of the form X,Y and translates the
    image by the displacement. The shape of the output is the same as the
    input, with missing pixels filled in with zeros."""
    num_rows = image.shape[0]
    num_cols = image.shape[1]
    pts = (np.mgrid[:num_rows, :num_cols]
           .transpose(1, 2, 0)
           .astype(np.float32))

    pts -= displacement[::-1]

    return bilinear_interp(image, pts)


def to_cylindrical(image, camera_params, debug=False):
    F, k1, k2 = camera_params
    num_rows = image.shape[0]
    num_cols = image.shape[1]
    cyl_img_pts = (np.mgrid[:num_rows, :num_cols]
                   .transpose(1, 2, 0)
                   .astype(np.float32))

    x_cyl = cyl_img_pts[..., 1]
    y_cyl = cyl_img_pts[..., 0]

    x_mid = num_cols / 2
    y_mid = num_rows / 2
    if debug:
        print_point("cyl", num_cols, num_rows, 0.25, x_cyl, y_cyl)
        print_point("cyl", num_cols, num_rows, 0.5, x_cyl, y_cyl)
        print_point("cyl", num_cols, num_rows, 0.75, x_cyl, y_cyl)

    # Convert from cylindrical image points x_cyl/y_cyl to cylindrical
    # coordinates theta/h.
    theta_cyl, h_cyl = to_cylidrical_coor(x_cyl, y_cyl, num_cols, num_rows, F)
    if debug:
        print "theta_mid: " + str(theta_cyl[0][x_mid])
        print "h_cyl: " + str(h_cyl[y_mid][0])

    # Convert from cylindrical coordinates theta/h to x/y/z coordinates on the
    # 3D cylinder.
    x_hat, y_hat, z_hat = to_xyz_coor(theta_cyl, h_cyl, num_cols, num_rows, F)

    if debug:
        print_point("hat", num_cols, num_rows, 0.25, x_hat, y_hat)
        print_point("hat", num_cols, num_rows, 0.5, x_hat, y_hat)
        print_point("hat", num_cols, num_rows, 0.75, x_hat, y_hat)

    # Convert from cylinder x/y/z to normalized input image x/y coordinates.
    norm_x_coor, norm_y_coor = (to_norm_coor(F, num_cols, num_rows,
                                x_hat, y_hat, z_hat))

    if debug:
        print_point("norm", num_cols, num_rows, 0.25, norm_x_coor, norm_y_coor)
        print_point("norm", num_cols, num_rows, 0.5, norm_x_coor, norm_y_coor)
        print_point("norm", num_cols, num_rows, 0.75, norm_x_coor, norm_y_coor)

    # Apply radial distortion correction to the normalized image x/y
    # coordinates.
    dist_x_coor, dist_y_coor = (apply_radial_distortion(norm_x_coor,
                                                        norm_y_coor,
                                                        k1,
                                                        k2))

    if debug:
        (print_point("distorted", num_cols, num_rows,
                     0.25, dist_x_coor, dist_y_coor))
        (print_point("distorted", num_cols, num_rows,
                     0.5, dist_x_coor, dist_y_coor))
        (print_point("distorted", num_cols, num_rows,
                     0.75, dist_x_coor, dist_y_coor))

    # Convert from normalized image x/y coordinates to actual x/y coordinates.
    act_x_coor, act_y_coor = (to_actual_coor(dist_x_coor,
                                             dist_y_coor,
                                             num_cols,
                                             num_rows,
                                             F))

    if debug:
        print_point("actual", num_cols, num_rows, 0.25, act_x_coor, act_y_coor)
        print_point("actual", num_cols, num_rows, 0.5, act_x_coor, act_y_coor)
        print_point("actual", num_cols, num_rows, 0.75, act_x_coor, act_y_coor)

    # Look up pixels in the input i at the final coordinates (using bilinear
    # interpolation) to form the cylindrical image and return it.

    return (bilinear_interp(image,
                            np.dstack((act_y_coor + num_rows / 2,
                                       act_x_coor + num_cols / 2))))


def to_cylidrical_coor(x, y, width, height, f):
    theta = (x - (width / 2)) / f
    h = (y - (height / 2)) / f

    return theta, h


def to_xyz_coor(theta_cyl, h_cyl, width, height, f):
    x_hat = np.sin(theta_cyl)
    y_hat = h_cyl
    z_hat = np.cos(theta_cyl)

    return x_hat, y_hat, z_hat


def to_norm_coor(f, width, height, x_hat, y_hat, z_hat):
    norm_x_coor = x_hat / z_hat
    norm_y_coor = y_hat / z_hat

    return norm_x_coor, norm_y_coor


def apply_radial_distortion(x_coor, y_coor, k1, k2):
    r_squared = x_coor**2 + y_coor**2
    r_quad = r_squared**2
    scale = (1 + k1 * r_squared + k2 * r_quad)
    dist_x_coor = x_coor * scale
    dist_y_coor = y_coor * scale

    return dist_x_coor, dist_y_coor


def to_actual_coor(x_tilda, y_tilda, width, height, f):
    act_x = f * x_tilda
    act_y = f * y_tilda

    return act_x, act_y


def print_point(label, width, height, scale, x, y):
    x_pt = x[0][width * scale]
    y_pt = y[height * scale][0]
    print label + "_x_coor_" + str(scale) + ": " + str(x_pt)
    print label + "_y_coor_" + str(scale) + ": " + str(y_pt)


def lucas_kanade(H, I, debug=False):
    """Given images H and I, compute the displacement that should be applied to
    H so that it aligns with I. H and I are assumed to be grayscale images."""

    if len(H.shape) > 2 or len(I.shape) > 2:
        raise ValueError("H and I are expected to be grayscale images"
                         " with only 2 dimensions")

    # Generate a binary mask indicating pixels that are valid (non-black) in
    # both H and I.
    bin_mask_H = np.greater(H, 0)
    bin_mask_I = np.greater(I, 0)
    mask = bin_mask_H * bin_mask_I

    # logical 'AND' the mask with another mask that selects pixels
    # that aren't dark (intensity > 0.25).
    bin_mask_H = bin_mask_H * np.greater(H, 0.25)
    bin_mask_I = bin_mask_I * np.greater(I, 0.25)
    mask = bin_mask_H * bin_mask_I

    # Compute the partial image derivatives w.r.t. X, Y, and Time (t).
    # I sub t is just the pixel value difference between 2 images
    It = I - H
    if debug:
        imsave("output/lucas_kanade/It.png", It)

    sobel_x = np.ndarray([3, 3], dtype=np.float32)
    sobel_x[:] = [[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]]
    Ix = (correlate(I, sobel_x) / 8.0)
    if debug:
        imsave("output/lucas_kanade/Ix.png", Ix)

    sobel_y = np.ndarray([3, 3], dtype=np.float32)
    sobel_y[:] = [[1, 2, 1], [0, 0, 0], [-1, -2, -1]]

    Iy = (correlate(I, sobel_y) / 8.0)
    if debug:
        imsave("output/lucas_kanade/Iy.png", Iy)

    # Compute the various products (Ixx, Ixy, Iyy, Ixt, Iyt) necessary to form
    # AtA. Apply the mask to each product to select only valid values.
    def sum_product(n, m):
        masked_n = ma.masked_array(n, mask)
        masked_m = ma.masked_array(m, mask)
        return np.add.reduce(np.add.reduce(np.multiply(masked_n, masked_m)))

    Ixx = sum_product(Ix, Ix)
    Ixy = sum_product(Ix, Iy)
    Iyy = sum_product(Iy, Iy)
    Ixt = sum_product(Ix, It)
    Iyt = sum_product(Iy, It)

    if debug:
        print "Ixx: ", Ixx
        print "Ixy: ", Ixy
        print "Iyy: ", Iyy
        print "Ixt: ", Ixt
        print "Iyt: ", Iyt

    # Build the AtA matrix and Atb vector
    AtA = np.ndarray((2, 2), dtype=np.float32)
    AtA[:] = [[Ixx, Ixy], [Ixy, Iyy]]
    Atb = np.ndarray((2, 1), dtype=np.float32)
    Atb[:] = [[Ixt], [Iyt]]

    # Solve the system and return the computed displacement.
    displacement = np.dot(np.linalg.inv(AtA), Atb)
    return displacement.flatten()


def iterative_lucas_kanade(H, I, steps, debug=False):
    # Run the basic Lucas Kanade algorithm in a loop `steps` times.
    # Start with an initial displacement of 0 and accumulate displacements.

    current_d = (0, 0)
    displacements = []

    for i in xrange(steps):
        # Translate the input image by the current displacement,
        # then run Lucas Kanade and update the displacement.

        if debug:
            print "Iterative Lucas Kanade -- step " + str(i)

        shifted_H = translate(H, current_d)
        if debug:
            name = "H" + str(i) + ".png"
            path = "output/iterative_lucas_kanade/" + name
            imsave(path, shifted_H)
        shifted_H_gray = to_grayscale(shifted_H)
        I_gray = to_grayscale(I)
        current_displacement = lucas_kanade(shifted_H_gray, I_gray, debug)
        if debug:
            print "[u, v] = ", current_d
        displacements.append(current_d)

    # Return the final displacement
    return current_displacement


def to_grayscale(image):
    temp = np.add.reduce(image, axis=2)
    final = np.divide(temp, 3)
    return final


def gaussian_pyramid(image, levels):
    # Build a Gaussian pyramid for an image with the given number of levels,
    # then return it.

    results = []

    filter_scale = 6

    temp = image
    for level in xrange(levels):
        std_dev = 2 * (level + 1)
        filter_size = std_dev * filter_scale
        h_kernel = np.ones((filter_size, 1, 1), dtype=np.float32)
        v_kernel = np.ones((1, filter_size, 1), dtype=np.float32)

        for i in xrange(h_kernel.shape[0]):
            exponent = (-1 / 2) * ((i**2) / std_dev**2)
            value = math.pow(math.e, exponent)
            value *= 1 / (2 * math.pi * std_dev**2)
            h_kernel[i, 0] = [value]

        for j in xrange(v_kernel.shape[1]):
            exponent = (-1 / 2) * ((j**2) / std_dev**2)
            value = math.pow(math.e, exponent)
            value *= 1 / (2 * math.pi * std_dev**2)
            v_kernel[0, j] = [value]

        origin_pt = (-1 * filter_size / 2, 0, 0)
        temp = (convolve(temp,
                         h_kernel,
                         mode="nearest", cval=1.0, origin=origin_pt))
        origin_pt = (0, -1 * filter_size / 2, 0)
        temp = (convolve(temp,
                         v_kernel,
                         mode="nearest", cval=1.0, origin=origin_pt))

        scale = 2**(level + 1)
        results.append(temp[0::scale, 0::scale, :])

    return results


def pyramid_lucas_kanade(H, I, initial_d, levels, steps, debug=False):
    """Given images H and I, and an initial displacement that roughly aligns H
    to I when applied to H, run Iterative Lucas Kanade on a pyramid of the
    images with the given number of levels to compute the refined
    displacement."""

    # Build Gaussian pyramids for the two images.
    # Coarsest image is the last in the list
    # levels of gaussian pyramid are [1/2, 1/4, 1/8, 1/16]
    H_gauss_pyramid = gaussian_pyramid(H, levels)
    I_gauss_pyramid = gaussian_pyramid(I, levels)

    # Start with the initial displacement, scaled to the coarsest level of the
    # pyramid, and compute the updated displacement at each level using
    # Iterative Lucas Kanade.
    global_displacement = initial_d.copy()
    for level in xrange(levels - 1, -1, -1):
        if debug:
            print "Pyramid Lucas Kanade -- level " + str(level)

        # Get the two images for this pyramid level.
        H_level = H_gauss_pyramid[level]
        I_level = I_gauss_pyramid[level]

        if debug:
            print "global_displacement: ", global_displacement

        # Scale the previous level's global displacement properly and apply it
        # to the second image (translate the image).
        scale = 2**(level + 1)
        scaled_d = global_displacement / scale

        if debug:
            print "scaled displacement:", scaled_d

        H_shifted = translate(H_level, scaled_d)

        if debug:
            path = "output/pyramid_lucas_kanade/H" + str(level) + ".png"
            imsave(path, H_level)

            path = "output/pyramid_lucas_kanade/H" + str(level) + "_shift.png"
            imsave(path, H_shifted)

            path = "output/pyramid_lucas_kanade/I" + str(level) + ".png"
            imsave(path, I_level)

        # Use the iterative Lucas Kanade method to compute a displacement
        # between the two images at this level.
        iterative_d = iterative_lucas_kanade(H_shifted, I_level, steps, debug)

        # Update the global displacement based on the one you just computed.
        global_displacement += (iterative_d * scale)

        if debug:
            print "displacement at level(" + str(level) + ")", iterative_d
            print "scale at level(" + str(level) + ")", scale

    return global_displacement


def build_panorama(images, shape, displacements, initial_position,
                   blend_width=32):
    # Allocate an empty floating-point image with space to store the panorama
    # with the given shape.
    panorama = np.zeros(shape, dtype=np.float32)
    imsave("output/panorama/blank.png", panorama)

    # Place the last image, warped to align with the first, at its proper place
    # to initialize the panorama.
    last_image = images[-1]
    last_displacement = displacements[-1]
    x_offset = math.floor(last_displacement[0])
    y_offset = math.floor(last_displacement[1])
    width = last_image.shape[1]
    height = last_image.shape[0]

    from_height = initial_position[1] + y_offset
    to_height = from_height + height

    from_width = initial_position[0]
    to_width = from_width + width + x_offset

    panorama[from_height:to_height, from_width:to_width] = last_image[:, abs(x_offset):]
    imsave("output/panorama/step_one.png", panorama)

    # Place the images at their final positions inside the panorama, blending
    # each image with the panorama in progress. Use a blending window with the
    # given width.
    blend = np.linspace(0.0, 1.0, num=32)
    initial_position = (initial_position[0] + width + x_offset,
                        initial_position[1] + y_offset)
    print "starting position: ", initial_position
    for i in xrange(len(images) - 1):
        print "Adding Image ", (i + 1)
        image = images[i]
        x_offset = int(math.floor(displacements[i][0]))
        y_offset = int(math.floor(displacements[i][1]))

        for x in xrange(image.shape[1]):
            for y in xrange(image.shape[0]):

                if x >= image.shape[1] or y >= image.shape[0]:
                    continue

                target_x = initial_position[0] + x_offset + x
                target_y = y
                target_shape = panorama.shape

                if target_x >= target_shape[1] or target_y >= target_shape[0]:
                    continue

                source_pt = image[y, x]
                target_pt = panorama[target_y, target_x]

                final_pt = None
                if x < (abs(x_offset) - 16):
                    final_pt = target_pt
                elif x >= (abs(x_offset) + 15):
                    final_pt = source_pt
                else:
                    index = x - (abs(x_offset) - 17)
                    final_pt = ((target_pt * (1.0 - blend[index])) +
                                (source_pt * blend[index]))

                panorama[target_y, target_x] = final_pt

        initial_x = initial_position[0] + image.shape[1] + x_offset
        initial_position = (initial_x,
                            initial_position[1] + y_offset)
        pass

    # Return the finished panorama.
    imsave("output/panorama/step_two.png", panorama)
    return panorama

# Displacements are by default saved to a file after every run. These flags
# control loading and saving of the displacements. Once you have confirmed your
# LK code is working, you can load saved displacements to save time testing the
# rest of the project.
SAVE_DISPLACEMENTS = True
LOAD_DISPLACEMENTS = False


def mosaic(images, initial_displacements, debug=True):
    """Given a list of N images taken in clockwise order and corresponding
    initial X/Y displacements of shape (N,2), refine the displacements and
    build a mosaic.


    initial_displacement[i] gives the translation that should be appiled to
    images[i] to align it with images[(i+1) % N]."""
    filename = "final_displacements.pkl"
    levels = 4
    steps = 5
    N = len(images)

    if LOAD_DISPLACEMENTS:
        print("Loading saved displacements...")
        final_displacements = pickle.load(open(filename, "rb"))
    else:
        print("Refining displacements with Pyramid Iterative Lucas Kanade...")
        final_displacements = []
        for i in xrange(N):
            # Use Pyramid Iterative Lucas Kanade to compute displacements from
            # each image to the image that follows it, wrapping back around at
            # the end. A suggested number of levels and steps is 4 and 5
            # respectively. Make sure to append the displacement to
            # final_displacements so it gets saved to disk if desired.

            initial_d = initial_displacements[i]

            # H = first image
            I = images[i]

            # I = second image
            H = images[(i + 1) % N]

            final_d = (pyramid_lucas_kanade(H, I,
                                            initial_d, levels, steps,
                                            debug))
            final_displacements.append(final_d)

            # Some debugging output to help diagnose errors.
            initial_translated_img = translate(H, -initial_d)
            initial_diff = abs((I - initial_translated_img)).mean()

            final_translated_img = translate(H, -final_d)
            final_diff = abs((I - final_translated_img)).mean()
            print("Image %d:" % i, initial_d, "->", final_d, "  ",
                  "%0.4f" % initial_diff, "->", "%0.4f" % final_diff)

        if SAVE_DISPLACEMENTS:
            pickle.dump(final_displacements, open(filename, "wb"))

    # Use the final displacements and the images' shape compute the full
    # panorama shape and the starting position for the first panorama image.
    starting_x = 0
    starting_y = 0
    final_height = 0
    final_width = 0
    for i in xrange(1, N):
        # remember shape[0] = height, shape[1] = width
        # and displacements are (x, y)
        img_shape = images[(i + 1) % N].shape

        x_offset = math.floor(final_displacements[i][0])
        y_offset = math.floor(final_displacements[i][1])

        final_width += img_shape[1] + x_offset
        final_height = max(final_height, img_shape[0] + abs(y_offset))

        if y_offset < 0:
            starting_y = max(starting_y, abs(y_offset))

    print "Final Size (height, width): ", final_height, final_width

    # Build the panorama.
    print("Building panorama...")
    starting_position = (starting_x, starting_y)
    final_shape = np.array((final_height, final_width, 3))
    panorama = (build_panorama(images,
                               final_shape,
                               final_displacements, starting_position))

    # Resample the panorama image using a linear warp to
    # distribute any vertical drift to all of the sampling points.
    # The final height of the panorama should be equal to the
    # height of one of the images.
    print("Warping to correct vertical drift...")
    print("----- Not completed ----")

    # Crop the panorama horizontally so that the left and right edges of the
    # panorama match (making it form a loop).

    # Return your final corrected panorama.
    return panorama
