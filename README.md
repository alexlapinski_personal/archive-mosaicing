https://www.cs.drexel.edu/~kon/introcompvis/assignments/project2/

# Get Started
 * Make sure python 2.7 is available
 * Make sure pip is available
 * Execute pip -r requirements.txt
 * Install OpenCV 2.X
  * On OSX: https://jjyap.wordpress.com/2014/05/24/installing-opencv-2-4-9-on-mac-osx-with-python-support/
 * Run Python files
