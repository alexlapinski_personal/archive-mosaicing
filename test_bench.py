import sys
import os

from scipy.misc import *
from p2_skeleton import *

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print """
python test_bench.py <test_method> <image1_filepath> <image2_filepath> <initial_d_x> <initial_d_y>
  <test_method> = gaussian_pyramid, pyramid_lucas_kanade, to_grayscale,
                  lucas_kanade
  <image1_filepath> = path to an image to use as input
  <image2_filepath> = path to an image to use as input
  <initial_d_x> = Initial Displacement for pyramid_lucas_kanade (x axis)
  <initial_d_y> = Initial Displacement for pyramid_lucas_kanade (y axis)
"""
        exit(1)

    filepath = sys.argv[2]

    test_method = sys.argv[1]

    filename = os.path.basename(filepath)
    name, ext = os.path.splitext(filename)

    image = imread(filepath).astype(np.float32) / 255.

    if len(sys.argv) >= 4:
        filepath2 = sys.argv[3]
        image2 = imread(filepath2).astype(np.float32) / 255.

    output_path = os.path.join("output", test_method)

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    if test_method == "gaussian_pyramid":
        pyramid = gaussian_pyramid(image, 4)

        for i in xrange(len(pyramid)):
            pyramid_image = pyramid[i]
            image_name = name + "_" + str(i) + ext
            pyramid_image_filename = os.path.join(output_path, image_name)

            print "Saving image '" + pyramid_image_filename + "'"
            print image_name + "'s size = " + str(pyramid_image.shape)
            imsave(pyramid_image_filename, pyramid_image)
    elif test_method == "pyramid_lucas_kanade":
        if len(sys.argv) < 6:
            print "pyramid_lucas_kanade requires 6 params"
        initial_d = np.array((int(sys.argv[4]), int(sys.argv[5])))
        final_d = (pyramid_lucas_kanade(image,
                                        image2,
                                        initial_d,
                                        4, 5, True))
        print "Final Displacement: ", final_d
    elif test_method == "to_grayscale":
        grayscale_image = to_grayscale(image)
        image_name = name + "_grayscale" + ext
        image_filepath = os.path.join(output_path, image_name)
        print "Saving image '" + image_filepath + "'"
        imsave(image_filepath, grayscale_image)
    elif test_method == "lucas_kanade":
        H_gray = to_grayscale(image)
        I_gray = to_grayscale(image2)
        result = lucas_kanade(H_gray, I_gray, debug=True)
        print "[u, v] = ", result.T
    elif test_method == "iterative_lucas_kanade":
        result = iterative_lucas_kanade(image, image2, 5)
        print "[u, v] = ", result
    else:
        print "Unknown test method '", test_method, "'"
